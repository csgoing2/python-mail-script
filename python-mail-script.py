# Programmatically send Emails via Outlook - for ConAm daily reports
# v1: Generate Email and get it ready to send (complete)
# C:\Python310\python.exe
from datetime import date
from fileinput import filename
import win32com.client as win32
import os
import mysql.connector
import csv
# Get today's date - to be used in Subject Line and Attachment Name
date = date.today()
# Path to file. script will look for current iteration of this file in this directory!
filePath = "C:\\xampp\\htdocs\\emailSendv2"
# Dynamic file name - include today's date in file name for differentiation!
fileName = "ConAm Transfer Report" + ' - ' + str(date) + '.csv'
#Connect to DB
dbConn = mysql.connector.connect(
    host = "backend-a1.cfuw0i7qssi5.us-west-2.rds.amazonaws.com",
    #host = "backend-a1-cluster.cluster-cfuw0i7qssi5.us-west-2.rds.amazonaws.com",
    user = "lmroot",
    password = "zB!pOrW?Ve0",
    database = 'sjllms'
)
# Query (MySQL)
query = '''select
          property_name,
          contact_names.first,
          contact_names.last,
          pms_request_responses.property_id,
          REPLACE(REPLACE(pms_request_responses.request,'\n',''),',','') as request,
          REPLACE(REPLACE(pms_request_responses.response,'\n',''),',','') as response,
          IF(pms_request_responses.status_code='22','Success','Error') as status_code,
          pms_request_responses.created_at,
          IF(pms_request_responses.status_code = 23,  -- if it's a failure...
          mid(pms_request_responses.response, -- <--STR argument. mid() function arguments are mid(str,pos,len)
                      position('messageType="Error"'IN pms_request_responses.response)+20, -- <--POS argument of mid(). Position(substring IN string)
          position('</Message>'IN pms_request_responses.response)-position('messageType="Error"'in pms_request_responses.response)-20), -- <-- LEN argument of mid() Position(substring IN string)
          '') as ErrorMessage -- if False (not an error), display nothing / blank string
      from property
      inner join guest_cards on guest_cards.property_id = property.property_id and guest_cards.property_id like 'CMC%'
      inner join master_leads on master_leads.guest_card_id = guest_cards.id
      inner join pms_request_responses on convert(replace(pms_request_responses.thirdparty_id,'rmg',''), SIGNED) = master_leads.id
      and pms_request_responses.created_at > (DATE(NOW()) + INTERVAL -1 DAY)
      left join occupants on occupants.guest_card_id = guest_cards.id
      inner join contact_names on contact_names.name_contactable_id = occupants.id and contact_names.ref_contact_relationship_id = 43;
'''
# Execute Query
cur = dbConn.cursor()
cur.execute(query)
result = cur.fetchall()
# UTF-8 encoding for special characters (XML is in output)
fp = open(filePath + '\\' + fileName, 'w', newline='', encoding='utf-8')
myFile = csv.writer(fp)
myFile.writerows(result)
# PRINT QUERY OUTPUT FOR TESTING
# for row in result:
#     print(row)
# create Outlook Object instance
outlookApp = win32.Dispatch('Outlook.Application') 
# Create Email - formulate subject line, recipients, attachment, etc.
mailItem = outlookApp.CreateItem(0)
mailItem.BodyFormat = 2
mailItem.subject = "ConAm Transfer Report"+ ' - ' + str(date)
mailItem.To = 'ilm@conam.com'
# mailItem.CC = "jason8156@gmail.com"
mailItem.Body = '''Please see attached for your daily ConAm Transfer Report. \n \n Thank you!'''
mailItem.Attachments.Add(os.path.join(os.getcwd(), fileName))
# Print output to debug console in VS Code (Optional)
countRows = 0
for x in result:
    countRows += 1
print("Output " + str(countRows) + " rows to file: " + fileName)
print("File Location: " + filePath)
# Display Email
mailItem.Display()
# Send Email - Optional
# mailItem.Send()
